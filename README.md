# How to use:

If you do not have **librarian** or **vagrant-omnibus** installed, install it now

	 gem install librarian-chef

	 vagrant plugin install vagrant-omnibus

Afterwards you can download all cookbooks

	librarian-chef install

And fire up the VM

	vagrant up