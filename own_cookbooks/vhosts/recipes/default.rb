include_recipe "apache2"

web_app "totalbox" do
    server_name "totalbox.box.creativebrains.net"
    docroot "/var/www/totalbox/web"
    allow_override "All"
end

web_app "cfbb_totalbox_io" do
    server_name "cfbb.totalbox.io"
    docroot "/var/www/totalbox/web"
    allow_override "All"
end

web_app "dropzone" do
    server_name "dropzone.box.creativebrains.net"
    docroot "/var/www/dropzone/public"
    allow_override "All"
end

web_app "phpmyadmin" do
	server_name "pma.box.creativebrains.net"
	docroot "/opt/phpmyadmin"
end