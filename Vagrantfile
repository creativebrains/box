# encoding: utf-8

# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|


	config.vm.box = "precise64"
	config.vm.box_url = "http://files.vagrantup.com/precise64.box"
	config.ssh.forward_agent = true

    config.vm.provider "virtualbox" do |vb|
        vb.customize [
            "modifyvm", :id,
            "--memory","2048"
        ]
    end

    config.vm.network "private_network", ip: "10.10.2.2"
    config.vm.network "forwarded_port", guest: 80, host: 8080
    config.vm.network "forwarded_port", guest: 3306, host: 3306

    config.omnibus.chef_version = :latest

    config.vm.synced_folder "work/", "/var/www/", type: "nfs"
    #config.vm.synced_folder "work/", "/var/www/", owner: "vagrant", group: "vagrant", :mount_options => ["dmode=777","fmode=777"]

	config.vm.provision :chef_solo do |chef|
		chef.cookbooks_path = ["cookbooks", "own_cookbooks"]
		chef.add_recipe :apt
		chef.add_recipe 'build-essential'
		chef.add_recipe 'vim'
		chef.add_recipe 'git'
		chef.add_recipe 'mysql::server'

		chef.add_recipe 'apache2'
		chef.add_recipe 'php'
		chef.add_recipe 'vhosts'
		chef.add_recipe 'apache2::mod_php5'
		chef.add_recipe 'php::module_mysql'
		chef.add_recipe 'php::module_apc'
		chef.add_recipe 'php::module_curl'
		chef.add_recipe 'phpmyadmin'

		chef.add_recipe 'phpredis'

		chef.add_recipe 'redis-package::server'

		#chef.add_recipe 'python'
		chef.add_recipe 'nodejs'

		#chef.add_recipe 'phpmongo'
		#chef.add_recipe 'mongodb'

		chef.add_recipe 'composer'

        chef.add_recipe 'couchbase::server'

		chef.add_recipe 'symfony2_stuff'

		chef.json = {
			:mongodb => {
				:dbpath  => "/var/lib/mongodb",
				:logpath => "/var/log/mongodb",
				:port    => "27017"
			},
			:git     => {
				:prefix => "/usr/local"
			},
			:mysql   => {
				:bind_address			=> "127.0.0.1",
				:server_root_password   => "password",
				:server_repl_password   => "password",
				:server_debian_password => "password",
				:service_name           => "mysql",
				:basedir                => "/usr",
				:data_dir               => "/var/lib/mysql",
				:root_group             => "root",
				:mysqladmin_bin         => "/usr/bin/mysqladmin",
				:mysql_bin              => "/usr/bin/mysql",
				:conf_dir               => "/etc/mysql",
				:confd_dir              => "/etc/mysql/conf.d",
				:socket                 => "/var/run/mysqld/mysqld.sock",
				:pid_file               => "/var/run/mysqld/mysqld.pid",
				:grants_path            => "/etc/mysql/grants.sql"
			},
			:apache  => {
				:default_site_enabled   => "true",
				:dir                    => "/etc/apache2",
				:log_dir                => "/var/log/apache2",
				:error_log              => "error.log",
				:user                   => "www-data",
				:group                  => "www-data",
				:binary                 => "/usr/sbin/apache2",
				:cache_dir              => "/var/cache/apache2",
				:pid_file               => "/var/run/apache2.pid",
				:lib_dir                => "/usr/lib/apache2",
				:listen_ports           => [
				  "80"
				],
				:contact                => "ops@example.com",
				:timeout                => "300",
				:keepalive              => "On",
				:keepaliverequests      => "100",
				:keepalivetimeout       => "5",
				:servertokens           => "all"
			},
			:phpmyadmin => {
				:host                   => "127.0.0.1",
				:port                   => 3306,
				:username               => "root",
				:password               =>  "password",
				:hide_dbs               => ["information_schema", "mysql", "phpmyadmin", "performance_schema" ],
				:fpm                    => false,
				:blowfish_secret        => "31337313373133731337",
				:login_cookie_validity  => 3600 * 8
			},
            :couchbase => {
                :server => {
                    :username => "admin",
                    :password => "admin1337"
                }
            }

		}

	end

end
